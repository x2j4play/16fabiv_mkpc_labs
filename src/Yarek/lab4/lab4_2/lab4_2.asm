model small
.data							; Вычитание
	num1_1 db 52			; 1 байт без знака
	num1_2 db 231			; 1 байт без знака
	num2_1 db 89			; 1 байт с знаком
	num2_2 db 55			; 1 байт с знаком
	num3_1 dd 00AF4D57h			; N байт без знака
	num3_2 dd 83654ADFh			; N байт без знака
	num4_1 dd 2911FDAAh			; N байт с знаком
	num4_2 dd 004FD439h			; N байт с знаком
	res1 dd ?
	res2 dd ?
	len1		equ	2	; Неупакованные BCD
	bcd1_1 		db	4,5	
	bcd1_2		db	7,9
	bcd_sub1	db	2 dup (0)
	bcd2_1		dw	1298h			;упакованное число 
	bcd2_2		dw	4675h			;упакованное число 
	bcd_sub2	db	2 dup (0)
.code
start:
	mov ax, @data
	mov ds, ax 
	xor ax, ax
	; 1 байт без знака
	mov al, num1_1
	sub al, num1_2

	; 1 байт с знаком
	mov bl, num2_1
	neg al	; если num1 отрицательное
	sub bl, num2_2

	; N байт без знака
	mov ax, WORD PTR[num3_1]
	sub ax, WORD PTR[num3_2]
	mov WORD PTR[res1], ax
	mov ax, WORD PTR[num3_1+2]
	sbb ax, WORD PTR[num3_2+2]
	mov WORD PTR[res1+2], ax

	; N байт с знаком
	mov 	ax, WORD PTR[num4_1]
	neg 	ax	; если num1 отрицательное 
	sub 	ax, WORD PTR[num4_2]
	mov 	WORD PTR[res2], ax
	mov 	ax, WORD PTR[num4_1+2]
	neg 	ax	; если num1 отрицательное
	sbb 	ax, WORD PTR[num4_2+2]
	mov 	WORD PTR[res2+2], ax

	xor 	bx, bx
	xor 	ax, ax
	mov		cx, len1
up_bcd:				; неупакованные BCD числа
	mov		al, bcd1_1[bx]
	sbb		al, bcd1_2[bx]
	aas	
	mov		bcd_sub1[bx], al
	inc		bx
	loop	up_bcd
	jc		up_bcd_neg 	; Если вычитаемое больше уменьшаемого
	jmp		p_bcd
up_bcd_neg:
	xor 	bx, bx
	xor 	ax, ax
	mov		cx, len1
up_bcd_neg_loop:
	mov		al, bcd1_2[bx]
	sbb		al, bcd1_1[bx]
	aas	
	mov		bcd_sub1[bx], al
	inc		bx
	loop	up_bcd_neg_loop

p_bcd:				; упакрванные BCD числа
	xor bx, bx
	xor ax, ax

	mov	al, BYTE PTR[bcd2_1]
	sub	al, BYTE PTR[bcd2_2]
	das
	mov	BYTE PTR[bcd_sub2], al	;младшие упакованные цифры результата
	mov	al, BYTE PTR[bcd2_1+1]
	sbb	al, BYTE PTR[bcd2_2+1]
	das
	jc	p_bcd_neg		; Если вычитаемое больше уменьшаемого
	mov	BYTE PTR[bcd_sub2+1], al	;младшие упакованные цифры результата
	jmp exit
p_bcd_neg:
	xor bx, bx
	xor ax, ax

	mov	al, BYTE PTR[bcd2_2]
	sub	al, BYTE PTR[bcd2_1]
	das
	mov	BYTE PTR[bcd_sub2], al	;младшие упакованные цифры результата
	mov	al, BYTE PTR[bcd2_2+1]
	sbb	al, BYTE PTR[bcd2_1+1]
	das
	mov	BYTE PTR[bcd_sub2+1], al	;младшие упакованные цифры результата
	
exit:
	mov ax, 4C00h
	int 21h	
end start
