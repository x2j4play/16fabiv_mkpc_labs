model small
.data
.stack 256
.code
start:
	mov ax, @data
	mov ds, ax 
    xor ax, ax

    mov dx, 03h
    push dx
    pop bx
    pushf
    cmp dx, bx
    popf
    
    mov ax, 4C00h
	int 21h	
end start
