model small
.data
.stack 256
.code
start:
	mov ax, @data
	mov ds, ax 
    xor ax, ax
    
    mov cx, 0Eh
    push cx
    pop bx
    pushf
    cmp bx, cx
    popf
    
    mov ax, 4C00h
	int 21h	
end start
