model small
.data
    num1_1  db 9    ; Умножение без знака
    num1_2  db 17    ; 
    num2_1  db 4    ; Умножение с знаком
    num2_2  db -20    ; 
    num3_1  dw 66    ; Деление без знака
    num3_2  db 3    ; 
    num4_1  dw 42    ; Деление с знаком
    num4_2  db -20    ; 
.code
start:
	mov ax, @data
	mov ds, ax 
    xor ax, ax
    
    ; Умножение без знака
	mov al, num1_1
    mul num1_2
    ; Умножение с знаком
    mov al, num2_1
    ;neg al  ; Если num1 отрицательное
	imul num2_2
    ; Деление без знака
    mov ax, num3_1
    div num3_2
    ; Деление с знаком
    mov ax, num4_1
    ;neg ax ; Если num1 отрицательное
	idiv num4_2	

	mov ax, 4C00h
	int 21h	
end start
