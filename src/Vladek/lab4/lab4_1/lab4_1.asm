model small
.data							; Сложение
	num1_1 db 134			; 1 байт без знака
	num1_2 db 68			; 1 байт без знака
	num2_1 db 125			; 1 байт с знаком
	num2_2 db -45			; 1 байт с знаком
	num3_1 dd 001754DAh			; N байт без знака
	num3_2 dd 0FFAD65FDh			; N байт без знака
	num4_1 dd 00667584h			; N байт с знаком
	num4_2 dd 0ffCBAF98h			; N байт с знаком
	res1 dd ?
	res2 dd ?
	len1		equ	3	; Неупакованные BCD
	bcd1_1 		db	2,2,6	
	bcd1_2		db	7,4,8
	bcd_sum1	db	4 dup (0)
	bcd2_1		dw	4343h			;упакованное число 
	bcd2_2		dw	9100h			;упакованное число 
	bcd_sum2	db	3 dup (0)
.code
start:
	mov ax, @data
	mov ds, ax 
	xor ax, ax

	; 1 байт без знака
	mov al, num1_1
	add al, num1_2

	; 1 байт с знаком
	mov bl, num2_1
	;neg al	; если num1 отрицательное
	add bl, num2_2

	; N байт без знака
	mov ax, WORD PTR[num3_1]
	add ax, WORD PTR[num3_2]
	mov WORD PTR[res1], ax
	mov ax, WORD PTR[num3_1+2]
	adc ax, WORD PTR[num3_2+2]
	mov WORD PTR[res1+2], ax

	; N байт с знаком
	mov ax, WORD PTR[num4_1]
	neg ax	; если num1 отрицательное 
	add ax, WORD PTR[num4_2]
	mov WORD PTR[res2], ax
	mov ax, WORD PTR[num4_1+2]
	neg ax	; если num1 отрицательное
	add ax, WORD PTR[num4_2+2]
	mov WORD PTR[res2+2], ax

	xor bx, bx
	xor ax, ax
	mov	cx, len1
m1:			; неупакованные BCD числа
	mov	al, bcd1_1[bx]
	adc	al, bcd1_2[bx]
	aaa
	mov	bcd_sum1[bx], al
	inc	bx
	loop	m1
	adc	bcd_sum1[bx], 0

	xor bx, bx
	xor ax, ax
	
	mov	al, BYTE PTR[bcd2_1]
	add	al, BYTE PTR[bcd2_2]
	daa
	mov	BYTE PTR[bcd_sum2], al	;младшие упакованные цифры результата
	mov	al, BYTE PTR[bcd2_1+1]
	adc	al, BYTE PTR[bcd2_2+1]
	daa
	mov ah, 1
	jnc	$+6	;переход через команду, если результат <= 99
	mov	BYTE PTR[bcd_sum2+2], ah	;учет переноса при сложении (результат > 99)
	mov	BYTE PTR[bcd_sum2+1], al	;младшие упакованные цифры результата
	
exit:
	mov ax, 4C00h
	int 21h	
end start
