﻿						Заголовок
%TITLE "LR_2"
data segment					;объявление сегмента данных
						;определение строки для вывода
message db 'Laboratory work was completed by Ulyanovsky Vladislav','$'
data ends					;конец сегмента данных
stk segment stack				;объявление сегмента стека
db 256 dup ('?')				;резервировать под стек 256 байт
stk ends					;конец сегмента стека
code segment					;объявление сегмента кода
assume cs:code, ds:data, ss:stk
start:						;точка входа в программу
						;настроить DS на сегмент данных
	mov ax,data				;в AX адрес сегмента данных
	mov ds,ax				;в DS содержимое AX
						;вывести на экран символьную строку message
	lea dx,message				;настройка DX на message
	mov ah,09h				;функция DOS: вывод строки на экран
	int 21h					;вызов DOS ;вывод строки message на экран
						;стандартный выход из программы
exit:	mov ax, 4c00h				;функция DOS: выход из программы
	int 21h					;вызов DOS; останов программы
	code ends				;конец сегмента кода
	
						;Завершение программы
end start					;конец программы/точка входа
