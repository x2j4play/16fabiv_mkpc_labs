model small
.data
.stack 256
.code
start:
	mov ax, @data
	mov ds, ax 
    xor ax, ax
    
    mov ax, 13h
    push ax
    pop cx
    pushf
    cmp ax, cx
    popf
    
    mov ax, 4C00h
	int 21h	
end start
