model small
.data
    num1_1  db 7    ; Умножение без знака
    num1_2  db 42    ; 
    num2_1  db 10    ; Умножение с знаком
    num2_2  db -2    ; 
    num3_1  dw 8    ; Деление без знака
    num3_2  db 4    ; 
    num4_1  dw 100    ; Деление с знаком
    num4_2  db -2    ; 
.code
start:
	mov ax, @data
	mov ds, ax 
    xor ax, ax
    
    ; Умножение без знака
	mov al, num1_1
    mul num1_2
    ; Умножение с знаком
    mov al, num2_1
    ;neg al  ; Если num1 отрицательное
	imul num2_2
    ; Деление без знака
    mov ax, num3_1
    div num3_2
    ; Деление с знаком
    mov ax, num4_1
    ;neg ax ; Если num1 отрицательное
	idiv num4_2	

	mov ax, 4C00h
	int 21h	
end start
