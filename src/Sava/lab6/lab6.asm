model small
.data
.stack 256
.code
start:
	mov ax, @data
	mov ds, ax 
    xor ax, ax
    
    mov ax, 27h
    push ax
    pop bx
    pushf
    cmp ax, bx
    popf
    
    mov ax, 4C00h
	int 21h	
end start
