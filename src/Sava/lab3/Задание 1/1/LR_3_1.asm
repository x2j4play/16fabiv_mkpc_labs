model small
.data
str_1 db 'Assembler'
str_2 db 9 dup (' ')
full_pnt dd str_1
.code
start:
	mov	ax,@data	 ; ��������� ������� ds � ���������
	mov	ds,ax	 ; ������ ����� ������� ax
	xor	ax,ax	 ; ������� ax
	lea	si,str_1
	lea	di,str_2
	les	bx,full_pnt	 ;������ ��������� �� str1 � ���� es:bx
	mov	cx,9	 ;���������� ���������� ����� � cx
m1:
	mov	al,[si]
	mov	[di],al
	inc	si
	inc	di
loop	m1 		 ;���� �� ����� m1 �� ��������� ���� ��������
	exit:
	mov	ax,4c00h 	 ;����������� �����
	int	21h
end start