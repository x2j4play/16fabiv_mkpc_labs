;Программа преобразования двузначного шестнадцатеричного числа
;в двоичное представление с использованием команды xlat.
;Вход: исходное шестнадцатеричное число; вводится с клавиатуры.
;Выход: результат преобразования в регистре al.
model small
.data	; сегмент данных
message	db	 'Enter two hexadecimal digits:  $'
tabl	db	 48 dup (0),   0,1,2,3,4,5,6,7,8,9,  7 dup (0)
	db	 0Ah,0Bh,0Ch,0Dh,0Eh,0Fh,  26 dup (0)
	db	 0ah,0bh,0ch,0dh,0eh,0fh,   152 dup (0)
.stack 256h  	;сегмент стека
.code                        ;начало сегмента кода 
 main   proc   	;начало процедуры main
	mov          ax,@data	;физический адрес сегмента данных в регистр ax
	mov	ds,ax	;ax записываем в ds
	lea	bx,tabl	;загрузка адреса строки байт в регистр bx
	mov	ah,9
	mov	dx,offset message
	int	21h	;вывести приглашение к вводу
	xor	ax,ax	;очистить регистр ax
	mov	ah,1h	;значение 1h в регистр ah
	int	21h	;вводим первую цифру в al
	xlat		;перекодировка первого введенного символа в al 
	mov	dl,al
	shl	dl,4	;сдвиг dl влево для освобождения места для младшей цифры
	int	21h	;ввод второго символа в al
	xlat	;перекодировка второго введенного символа в al
	add	al,dl	;складываем для получения результата
	mov	ax,4c00h	;пересылка 4c00h в регистр ax 
	int	21h	;завершение программы 
 main   endp                              ;конец процедуры mainа 
 end    main	                ;конец программы с точкой входа main 

