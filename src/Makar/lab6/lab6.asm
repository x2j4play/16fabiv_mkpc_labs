model small
.data
.stack 256
.code
start:
	mov ax, @data
	mov ds, ax 
    xor ax, ax
    
    mov bx, 54h
    push bx
    pop ax
    pushf
    cmp bx, ax
    popf
    
    mov ax, 4C00h
	int 21h	
end start
